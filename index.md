# Bughunting

Show the world that you are a great hacker!

BugHunting is a challenge for participants with the aim for them to
try the daily work of a package (or open-source project) maintainer.
This is done by participants getting a broken code, along with a bug
report, and them submitting the code fixes (= Task).

The purpose of every task is to find the bug, and fix it in the code. Your solution is evaluated in real-time after you submit it, and you score points if your solution is correct. Some of the issues are artificial and were created only for the Challenge, while some are real-life issues.

## Watch a Quick Introduction on YouTube

[![BugHunting introduction for Red Hat Open House](http://img.youtube.com/vi/GvG-4nYUV-c/0.jpg)](http://www.youtube.com/watch?v=GvG-4nYUV-c "BugHunting introduction for Red Hat Open House")

If you like this kind of puzzles and would like to do this for the living, leave your contact to our Talent acquisition team at <https://jobs.redhat.com>.

## Bughunting organization

Bughunting is organized as a single session with many tasks with various difficulty. More difficult tasks are for more points. We advise you to start with the easier ones, as more advanced tasks can be quite tricky.

Tasks are available in a container that includes all required dependencies to build and run the tasks, see "About environment setup" below to learn how to pull and run the container.

## Rules

* You'll get the Codename in our session, use this to sign into <http://bughunting.cz> where you will get instructions on how to pull and run the container.
* Each task has an entry on <http://bughunting.cz>, where you can read more about instructions
  on how to build the task, what is the problem, how to reproduce and what is the expected behaviour.
* Points are awarded upon correct solution.
* The solution can be a dirty fix, but you must not break the basic functionality of the application.
* Tasks have **hints** to help you find the issue. Each time you uncover a hint, the system lowers the maximum number of points you can get for the task.
* Winners are announced at the conference end.

## How to submit your fix

To submit the task for verification, run the command `hunt` in the terminal, inside the task directory in the container. You can submit every task as many times as you want.

```console
hunt check warm_up
```

## In detail

[About environment setup](env_setup.md)

[How to submit a task](submit_task.md)

[Where are tasks located](task_location.md)

[What do the commands on the welcome screen do](commands.md)

[Questions and Answers](questions_and_answers.md)

[Do you have an idea for an interesting task?](task_idea.md)
