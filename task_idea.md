## Do you have an idea for an interesting task?

If you have an idea for an interesting task, please contact us through [GitLab issues](https://gitlab.com/bughunting/tasks/-/issues/new) and share your idea!

The issue should contain atleast a description of the task's basic functionality and how that functionality was broken.

[Back](index.html)
